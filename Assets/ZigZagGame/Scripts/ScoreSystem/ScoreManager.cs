using System;
using UnityEngine;

public class ScoreManager
{
    private const string BestScoreKey = "BestScore";

    

    /// <summary>
    /// Рекорд за все время игры
    /// </summary>
    public int BestScore
    {
        get
        {
            var hasKey = PlayerPrefs.HasKey(BestScoreKey);
            return hasKey ? PlayerPrefs.GetInt(BestScoreKey) : 0;
        }
        set { PlayerPrefs.SetInt(BestScoreKey, value); }
    }

    
    
    private int _score;
    
    /// <summary>
    /// Текущее количество очков игрока
    /// </summary>
    public int Score
    {
        get { return _score; }
        set
        {
            _score = value;
            OnScoreChange(value);
        }
    }

    public event Action<int> ScoreChanged;

    private void OnScoreChange(int value)
    {
        if (ScoreChanged != null)
            ScoreChanged(value);
    }

    public void UpdateBestScore()
    {
        if (Score > BestScore) BestScore = Score;
    }
}