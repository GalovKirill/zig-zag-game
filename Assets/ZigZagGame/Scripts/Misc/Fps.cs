using UnityEngine;

public class Fps : MonoBehaviour
{
    private void OnGUI()
    {
        var rect = new Rect(Screen.width / 2,Screen.height - 200, 100,100 );
        GUI.Label(rect, (1f / Time.deltaTime).ToString());
    }
}