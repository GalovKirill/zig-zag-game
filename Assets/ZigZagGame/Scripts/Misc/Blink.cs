using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Создает эффект мрограния текстового компонента
/// </summary>
public class Blink : MonoBehaviour
{
    [SerializeField] private float _frequency = 1f;
    [SerializeField] private Text _text;

    private void Update()
    {
        if(!_text) return;
        
        var color = _text.color;
        color.a = (Mathf.Sin(Time.time * _frequency) + 1) / 2f;
        _text.color = color;
    }
}