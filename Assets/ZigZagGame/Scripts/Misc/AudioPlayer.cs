using UnityEngine;

public class AudioPlayer
{
    private readonly Camera _camera;

    public AudioPlayer(Camera camera)
    {
        _camera = camera;
    }

    public void Play(AudioClip clip)
    {
        const float volume = 1f;
        Play(clip, volume);
    }

    public void Play(AudioClip clip, float volume)
    {
        _camera.GetComponent<AudioSource>().PlayOneShot(clip, volume);
    }
}