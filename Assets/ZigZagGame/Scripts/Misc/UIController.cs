﻿using System;
using a;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class UIController : MonoBehaviour
{
    [SerializeField] private Text _bestScoreTextField;
    private GameController _gameController;
    [SerializeField] private Text _gameOverLabel;
    [SerializeField] private Text _gameScoreTextField;
    [SerializeField] private Text _hintToRestartTextField;
    [SerializeField] private Text _hintToStartTextField;

    [SerializeField] private Text _totalScoreTextField;

    private ScoreManager _scoreManager;

    public int TotalScore
    {
        set { _totalScoreTextField.text = "Total score: " + value; }
    }

    public int GameScore
    {
        set { _gameScoreTextField.text = "Score: " + value; }
    }

    public int BestScore
    {
        set { _bestScoreTextField.text = "Best score: " + value; }
    }

    [Inject]
    public void Construct(ScoreManager scoreManager, GameController gameController)
    {
        _scoreManager = scoreManager;
        _gameController = gameController;
    }

    private void OnEnable()
    {
        _scoreManager.ScoreChanged += OnScoreChange;
        _gameController.GameStateChanged += OnGameStateChange;
    }

    private void OnGameStateChange(GameStates newState)
    {
        switch (newState)
        {
            case GameStates.WaitToStart:
                SetActiveGameResults(false);
                SetActiveStartHint(true);
                break;
            case GameStates.Playing:
                SetActiveStartHint(false);
                SetActiveGameScore(true);
                break;
            case GameStates.GameOver:
                TotalScore = _scoreManager.Score;
                BestScore = _scoreManager.BestScore;
                SetActiveGameScore(false);
                SetActiveGameResults(true);
                break;
            default:
                throw new ArgumentOutOfRangeException("newState", newState, null);
        }
    }

    private void OnScoreChange(int newScore)
    {
        GameScore = newScore;
    }

    public void SetActiveGameResults(bool value)
    {
        _totalScoreTextField.gameObject.SetActive(value);
        _gameOverLabel.gameObject.SetActive(value);
        _bestScoreTextField.gameObject.SetActive(value);
        _hintToRestartTextField.gameObject.SetActive(value);
    }

    public void SetActiveStartHint(bool value)
    {
        _hintToStartTextField.gameObject.SetActive(value);
    }

    public void SetActiveGameScore(bool value)
    {
        _gameScoreTextField.gameObject.SetActive(value);
    }
}