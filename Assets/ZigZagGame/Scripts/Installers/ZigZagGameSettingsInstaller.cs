using UnityEngine;
using Zenject;

[CreateAssetMenu(fileName = "ZigZagSettingsInstaller", menuName = "Installers/ZigZagSettingsInstaller")]
public class ZigZagGameSettingsInstaller : ScriptableObjectInstaller<ZigZagGameSettingsInstaller>
{
    [SerializeField] private PlayerMoveHandler.Settings _moveSettings;
    [SerializeField] private ZigZagGameInstaller.Settings _settingsInstall;
    [SerializeField] private TileCreationAssistant.Settings _tileCreationAssistanceSettings;
    [SerializeField] private BonusCreationAssistant.Settings _bonusCreationAssistanceSettings;
    [SerializeField] private CrystalDieHandler.Settings _crystalDieHandlerSettings;

    public override void InstallBindings()
    {
        Container.BindInstance(_moveSettings);
        Container.BindInstance(_settingsInstall);
        Container.BindInstance(_tileCreationAssistanceSettings);
        Container.BindInstance(_bonusCreationAssistanceSettings);
        Container.BindInstance(_crystalDieHandlerSettings);
    }
}