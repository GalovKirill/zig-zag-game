using System;
using a;
using UnityEngine;
using Zenject;

public class ZigZagGameInstaller : MonoInstaller
{
    [Inject] private Settings _settings;

    public override void InstallBindings()
    {
        LevelGeneratorInstall();
        PlayerInstall();
        BonusInstall();
        MiscInstall();
        MainInstall();
    }

    private void MiscInstall()
    {
        Container.BindInterfacesAndSelfTo<ScoreManager>().AsSingle();
        Container.BindInterfacesAndSelfTo<AudioPlayer>().AsSingle();
    }

    private void MainInstall()
    {
        Container.BindInterfacesAndSelfTo<GameController>().AsSingle();
        Container.BindInterfacesAndSelfTo<GameStateFactory>().AsSingle();
        Container.BindFactory<WaitToStartState, WaitToStartState.Factory>().AsSingle();
        Container.BindFactory<PlayingState, PlayingState.Factory>().AsSingle();
        Container.BindFactory<GameOverState, GameOverState.Factory>().AsSingle();
    }

    private void BonusInstall()
    {
        Container.BindFactory<Crystal, Crystal.Factory>().FromComponentInNewPrefab(_settings.CrystalBonusPrefab)
            .WithGameObjectName("CrystalBonus").AsSingle();
        Container.BindInterfacesAndSelfTo<BonusFactory>().AsSingle();
    }

    private void LevelGeneratorInstall()
    {
        Container.BindInterfacesAndSelfTo<LevelGenerator>().AsSingle();
        Container.BindInterfacesAndSelfTo<TileCreationAssistant>().AsSingle();
        Container.BindInterfacesAndSelfTo<BonusCreationAssistant>().AsSingle();
        Container.BindInterfacesAndSelfTo<TileFactory>().AsSingle();
        Container.BindFactory<CubeTile, CubeTile.Factory>().FromPoolableMemoryPool<CubeTile, CubeTile.CubeTilePool>(
            poolBinder =>
                poolBinder.WithInitialSize(50).FromComponentInNewPrefab(_settings.CubeTailPrefab)
                    .WithGameObjectName("CubeTile").UnderTransformGroup("Tiles"));
    }

    private void PlayerInstall()
    {
        Container.BindInterfacesAndSelfTo<DirectionSelector>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerInputNotifier>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerMoveHandler>().AsSingle();
        Container.BindInterfacesAndSelfTo<PlayerInputHandler>().AsSingle();
    }

    [Serializable]
    public class Settings
    {
        public GameObject CrystalBonusPrefab;
        public GameObject CubeTailPrefab;
    }
}