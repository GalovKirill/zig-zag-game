using System;
using UnityEngine;
using Zenject;

/// <summary>
/// Обработчик разрушения кристала
/// </summary>
public class CrystalDieHandler : MonoBehaviour
{
    private AudioPlayer _audioPlayer;
    private Settings _settings;

    [Inject]
    public void Construct(AudioPlayer audioPlayer, Settings settings)
    {
        _audioPlayer = audioPlayer;
        _settings = settings;
    }
    
    public void Die()
    {
        var particle = Instantiate(_settings.ParticlePrefab, transform.position, Quaternion.Euler(-90f, 0, 0));
        const float lifeTime = 2f;
        Destroy(particle, lifeTime);
        _audioPlayer.Play(_settings.BrokenCrystalSound);
        Destroy(this.gameObject);
    }
    
    [Serializable]
    public class Settings
    {
        public GameObject ParticlePrefab;
        public AudioClip BrokenCrystalSound;
    }
    
    
    
}