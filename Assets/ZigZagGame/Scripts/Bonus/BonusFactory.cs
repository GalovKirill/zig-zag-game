using Zenject;

namespace a
{
    public class BonusFactory : IFactory<Bonus>
    {
        private readonly Crystal.Factory _crystalFactory;

        public BonusFactory(Crystal.Factory crystalFactory)
        {
            _crystalFactory = crystalFactory;
        }

        public Bonus Create()
        {
            return _crystalFactory.Create();
        }
    }
}