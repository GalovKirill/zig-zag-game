using UnityEngine;
using Zenject;

public class Crystal : Bonus
{
    [SerializeField] private CrystalDieHandler _dieHandler;
    private const float CrystalHight = 0.6f;
    private ScoreManager _scoreManager;

    [Inject]
    public void Construct(ScoreManager scoreManager)
    {
        _scoreManager = scoreManager;
    }

    public override void SetPosition(Vector3 position)
    {
        Position = position + CrystalHight * Vector3.up;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            _scoreManager.Score++;
            _dieHandler.Die();
        }
    }

    public class Factory : PlaceholderFactory<Crystal>
    {
    }
}