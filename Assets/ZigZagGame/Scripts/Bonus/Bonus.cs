using UnityEngine;

/// <summary>
/// Базовый класс для всех бонусов
/// </summary>
public abstract class Bonus : MonoBehaviour
{
    public Vector3 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public abstract void SetPosition(Vector3 position);
}