using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private CharacterController _characterController;

    public Vector3 Velocity
    {
        get { return _characterController.velocity; }
    }

    public Vector3 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    public bool IsDead
    {
        get { return !_characterController.isGrounded; }
    }

    private void Start()
    {
        _characterController.Move(Physics.gravity);
    }

    public void Move(Vector3 moveOffset)
    {
        var moveVect = moveOffset + Physics.gravity;
        _characterController.Move(moveVect);
    }
}