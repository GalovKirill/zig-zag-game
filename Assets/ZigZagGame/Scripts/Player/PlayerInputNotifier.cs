using System;

/// <summary>
/// Класс, который является уведомителем смены направления движения игрока
/// </summary>
public class PlayerInputNotifier
{
    public event Action DirectionChanged;

    public void OnDirectionChange()
    {
        if (DirectionChanged != null)
            DirectionChanged();
    }
}