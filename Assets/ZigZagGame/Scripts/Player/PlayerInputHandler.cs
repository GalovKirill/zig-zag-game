using UnityEngine;
using Zenject;

/// <summary>
/// Обрабатывает ввод пользователя
/// </summary>
public class PlayerInputHandler : ITickable
{
    private readonly PlayerInputNotifier _playerInputNotifier;

    public PlayerInputHandler(PlayerInputNotifier playerInputState)
    {
        _playerInputNotifier = playerInputState;
    }

    public void Tick()
    {
#if UNITY_EDITOR
        if (Input.anyKeyDown)
        {
            _playerInputNotifier.OnDirectionChange();
            return;
        }
#endif

        if (Input.touchCount == 0)
            return;

        var touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Began) _playerInputNotifier.OnDirectionChange();
    }
}