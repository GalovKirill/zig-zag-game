using UnityEngine;

/// <summary>
/// Этот класс содержит логику выбора направления движения игрока и построения дороги
/// </summary>
public class DirectionSelector
{
    private Direction _direction = Direction.Forward;

    /// <summary>
    /// Функция возращает вектор движения в зависимости от его предыдущего направления
    /// </summary>
    public Vector3 GetDirection()
    {
        if (_direction == Direction.Forward)
        {
            _direction = Direction.Right;
            return Vector3.right;
        }

        _direction = Direction.Forward;
        return Vector3.forward;
    }

    public Direction GetRandomDirection()
    {
        return (Direction) Random.Range(0, 2);
    }
}

public enum Direction
{
    Right,
    Forward
}