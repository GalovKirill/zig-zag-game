using System;
using UnityEngine;
using Zenject;

/// <summary>
/// Этот класс урпавляет передвижением игрока(и камеры), регирует на команды ввода пользователя
/// </summary>
public class PlayerMoveHandler : IInitializable, ITickable, IDisposable
{
    private readonly DirectionSelector _directionSelector;
    private readonly Camera _mainCamera;
    private readonly Player _player;
    private readonly PlayerInputNotifier _playerInputState;
    private readonly Settings _settings;

    public PlayerMoveHandler(PlayerInputNotifier playerInputState, Player player, Settings settings,
        DirectionSelector directionSelector, Camera mainCamera)
    {
        _playerInputState = playerInputState;
        _player = player;
        _settings = settings;
        _directionSelector = directionSelector;
        _mainCamera = mainCamera;
    }

    public bool Enable { get; set; }

    /// <summary>
    /// Скорость перемещения игрока
    /// </summary>
    public float Speed
    {
        get { return _settings.Speed; }
    }

    /// <summary>
    /// Направление движения игрока
    /// </summary>
    public Vector3 MoveDirection { get; set; }

    public void Dispose()
    {
        _playerInputState.DirectionChanged -= OnDirectionChange;
    }


    public void Initialize()
    {
        _playerInputState.DirectionChanged += OnDirectionChange;
    }

    public void Tick()
    {
        if (!Enable)
            return;

        var movementVector = MoveDirection * Speed * Time.deltaTime;
        _player.Move(movementVector);
        _mainCamera.transform.position += movementVector;
    }

    private void OnDirectionChange()
    {
        MoveDirection = _directionSelector.GetDirection();
    }

    [Serializable]
    public class Settings
    {
        public float Speed;
    }
}