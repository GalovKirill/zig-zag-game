using System;
using Zenject;

/// <summary>
/// Класс, отвечабщий за обрабоку различных игровых состояний(паттерн State)
/// </summary>
public class GameController : IInitializable, ITickable
{
    private readonly GameStateFactory _stateFactory;
    
    /// <summary>
    /// Текущее состояние игры
    /// </summary>
    private GameState _state;

    public GameController(GameStateFactory stateFactory)
    {
        _stateFactory = stateFactory;
    }


    public void Initialize()
    {
        ChangeStateTo(GameStates.WaitToStart);
    }

    public void Tick()
    {
        _state.Update();
    }

    public event Action<GameStates> GameStateChanged;

    public void ChangeStateTo(GameStates state)
    {
        if (_state != null)
            _state.OnStateExit();
        _state = _stateFactory.Create(state);
        _state.OnStateEnter();
        OnGameStateChange(state);
    }

    private void OnGameStateChange(GameStates state)
    {
        if (GameStateChanged != null)
            GameStateChanged(state);
    }
}