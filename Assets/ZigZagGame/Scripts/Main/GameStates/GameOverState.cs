using UnityEngine;
using UnityEngine.SceneManagement;
using Zenject;

public class GameOverState : GameState
{
    private readonly ScoreManager _scoreManager;

    public GameOverState(GameController game, ScoreManager scoreManager) : base(game)
    {
        _scoreManager = scoreManager;
    }

    public override void OnStateEnter()
    {
        _scoreManager.UpdateBestScore();
    }

    public override void Update()
    {
#if UNITY_EDITOR
        if (Input.anyKeyDown)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            return;
        }
#endif
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Ended) SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    public override void OnStateExit()
    {
    }

    public class Factory : PlaceholderFactory<GameOverState>
    {
    }
}