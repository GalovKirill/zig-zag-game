using System;
using Zenject;

public class GameStateFactory : IFactory<GameStates, GameState>
{
    private readonly GameOverState.Factory _gameOverStateFactory;
    private readonly PlayingState.Factory _playingStateFactory;
    private readonly WaitToStartState.Factory _waitToStartStateFactory;

    public GameStateFactory(WaitToStartState.Factory waitToStartStateFactory,
        PlayingState.Factory playingStateFactory, GameOverState.Factory gameOverStateFactory)
    {
        _waitToStartStateFactory = waitToStartStateFactory;
        _playingStateFactory = playingStateFactory;
        _gameOverStateFactory = gameOverStateFactory;
    }

    public GameState Create(GameStates state)
    {
        switch (state)
        {
            case GameStates.WaitToStart:
                return _waitToStartStateFactory.Create();
            case GameStates.Playing:
                return _playingStateFactory.Create();
            case GameStates.GameOver:
                return _gameOverStateFactory.Create();
            default:
                throw new ArgumentOutOfRangeException("state", state, null);
        }
    }
}

public enum GameStates
{
    WaitToStart,
    Playing,
    GameOver
}