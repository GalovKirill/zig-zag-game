using UnityEngine;
using Zenject;

public class PlayingState : GameState
{
    private readonly LevelGenerator _levelGenerator;
    private readonly Player _player;
    private readonly PlayerMoveHandler _playerMoveHandler;

    public PlayingState(Player player, LevelGenerator levelGenerator, PlayerMoveHandler playerMoveHandler,
        GameController gameController) : base(gameController)
    {
        _player = player;
        _levelGenerator = levelGenerator;
        _playerMoveHandler = playerMoveHandler;
    }

    public override void OnStateEnter()
    {
        _playerMoveHandler.Enable = true;
        _playerMoveHandler.MoveDirection = Vector3.zero;
    }

    public override void Update()
    {
        _levelGenerator.UpdateLevel();
        if (_player.IsDead) Game.ChangeStateTo(GameStates.GameOver);
    }

    public override void OnStateExit()
    {
        _playerMoveHandler.Enable = false;
    }

    public class Factory : PlaceholderFactory<PlayingState>
    {
    }
}