using UnityEngine;
using Zenject;

public class WaitToStartState : GameState
{
    public WaitToStartState(GameController game) : base(game)
    {
    }

    public override void OnStateEnter()
    {
    }

    public override void Update()
    {
#if UNITY_EDITOR
        if (Input.anyKeyDown)
        {
            Game.ChangeStateTo(GameStates.Playing);
            return;
        }
#endif

        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Ended) Game.ChangeStateTo(GameStates.Playing);
        }
    }

    public override void OnStateExit()
    {
    }

    public class Factory : PlaceholderFactory<WaitToStartState>
    {
    }
}