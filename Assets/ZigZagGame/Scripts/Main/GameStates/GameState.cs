/// <summary>
/// Базовый класс для игровых состояний
/// </summary>
public abstract class GameState
{
    protected readonly GameController Game;

    protected GameState(GameController game)
    {
        Game = game;
    }

    public abstract void OnStateEnter();
    public abstract void Update();
    public abstract void OnStateExit();
}