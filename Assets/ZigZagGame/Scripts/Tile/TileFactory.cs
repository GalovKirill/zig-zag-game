using Zenject;

public class TileFactory : IFactory<Tile>
{
    private readonly CubeTile.Factory _cubeTIleFactory;

    public TileFactory(CubeTile.Factory cubeTIleFactory)
    {
        _cubeTIleFactory = cubeTIleFactory;
    }

    public Tile Create()
    {
        return _cubeTIleFactory.Create();
    }
}