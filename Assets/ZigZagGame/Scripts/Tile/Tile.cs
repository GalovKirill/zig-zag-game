using UnityEngine;

/// <summary>
/// Базовый класс для все тайлов
/// </summary>
public abstract class Tile : MonoBehaviour
{
    public Vector3 Position
    {
        get { return transform.position; }
        set { transform.position = value; }
    }

    /// <summary>
    /// Шаблон(паттерн template method) алгоритма соеденения двух тайлов, где определение точек соеденения двух тайлов
    /// делегированно наследникам класса Tile
    /// </summary>
    /// <param name="tile">Тайл с которым необходимо соеденится</param>
    /// <param name="direction">Направление построения дороги</param>
    public void ConnectTo(Tile tile, Direction direction)
    {
        var connectPoint = tile.GetConnectPoint(direction);
        Connect(connectPoint, direction);
    }

    /// <summary>
    /// Устанавливает бонус на тайл
    /// </summary>
    public abstract void SetBonus(Bonus bonus);

    /// <summary>
    /// Присоединяет тайл к connectPoint
    /// </summary>
    /// <param name="connectPoint">World space координаты точки соеденения тайлов</param>
    /// <param name="direction">Направление построения дороги</param>
    protected abstract void Connect(Vector3 connectPoint, Direction direction);

    /// <summary>
    /// Возвращает точку(координату плоскости), к которой может присоедениться другой тайл
    /// </summary>
    /// <param name="direction">Направление построения дороги</param>
    /// <returns>World space координата</returns>
    protected abstract Vector3 GetConnectPoint(Direction direction);
}