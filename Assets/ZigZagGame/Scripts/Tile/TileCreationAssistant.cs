using System;

/// <summary>
/// Этот класс проверяет необходимость создания новых тайлов
/// </summary>
public class TileCreationAssistant
{
    private readonly Player _player;
    private readonly Settings _settings;

    public TileCreationAssistant(Player player, Settings settings)
    {
        _player = player;
        _settings = settings;
    }

    /// <summary>
    /// Дистанция от игрока, на которой необходимо создавать тайлы
    /// </summary>
    private float TileCreateDistance
    {
        get { return _settings.TileCreateDistance; }
    }

    public bool IsNeedToCreateNewTile(Tile tile)
    {
        var playerIsCloseToTile =
            (tile.Position - _player.Position).sqrMagnitude < TileCreateDistance * TileCreateDistance;
        return playerIsCloseToTile;
    }

    [Serializable]
    public class Settings
    {
        public float TileCreateDistance;
    }
}