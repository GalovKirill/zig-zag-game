using UnityEngine;

public class TileFallHandler : MonoBehaviour
{
    private static Camera _camera;
    
    private static Camera Camera
    {
        get { return _camera ? _camera : _camera = Camera.main; }
    }

    [SerializeField] private Animator _animator;
    [SerializeField] private Renderer _renderer;
    
    /// <summary>
    /// Отступ от нижней части экрана, при котором запускается анимация падения тайла
    /// </summary>
    private float _bottomOffset = 0.25f;

    /// <summary>
    /// Истинно, если тайл находится в нижней части экрана
    /// </summary>
    public bool TileAtBottomOfScreen
    {
        get { return Camera.WorldToViewportPoint(transform.position).y < _bottomOffset; }
    }
    public bool IsFalling { get; private set; }

    private void OnEnable()
    {
        IsFalling = false; // реинициализация перевенной при возвращении из MemoryPool
    }

    private void Update()
    {
        if (_renderer.isVisible && !IsFalling && TileAtBottomOfScreen)
        {
            _animator.Play("Fall");
            IsFalling = true;
        }
    }
}