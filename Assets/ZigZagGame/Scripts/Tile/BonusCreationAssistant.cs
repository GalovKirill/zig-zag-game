using System;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

/// <summary>
/// Определяет когда необходимо создать бонус
/// </summary>
public class BonusCreationAssistant
{
    private readonly Settings _settings;

    public BonusCreationAssistant(Settings settings)
    {
        _settings = settings;
    }

    /// <summary>
    /// Шанс создания бонуса
    /// </summary>
    public float Chance
    {
        get { return _settings.Chance; }
    }

    public bool IsNeedToCreateBonus()
    {
        return Random.Range(0f, 1f) < Chance;
    }
    
    [Serializable]
    public class Settings
    {
        [Range(0f, 1f)] public float Chance;
    }
}