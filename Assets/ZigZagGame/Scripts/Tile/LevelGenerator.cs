using UnityEngine;
using Zenject;

public class LevelGenerator
{
    private readonly IFactory<Bonus> _bonusFactory;
    private readonly DirectionSelector _directionSelector;
    private readonly TileCreationAssistant _tileCreationAssistant;
    private readonly BonusCreationAssistant _bonusCreationAssistant;
    private readonly IFactory<Tile> _tileFactory;

  
    [Inject(Id = "first")] // Инжектится тайл, с которого начнется построение дороги.
    private Tile _lastCreatedTile; 

    public LevelGenerator(IFactory<Tile> tileFactory, DirectionSelector directionSelector,
        TileCreationAssistant tileCreationAssistant, IFactory<Bonus> bonusFactory, BonusCreationAssistant bonusCreationAssistant)
    {
        _tileFactory = tileFactory;
        _directionSelector = directionSelector;
        _tileCreationAssistant = tileCreationAssistant;
        _bonusFactory = bonusFactory;
        _bonusCreationAssistant = bonusCreationAssistant;
    }

    /// <summary>
    /// Функция провряет небходимость создания нового тайла, устанавливает его, создает бонус
    /// </summary>
    public void UpdateLevel()
    {
        if (NeedToCreateTile())
        {
            var newTile = _tileFactory.Create();
            var randomDirection = _directionSelector.GetRandomDirection();
            newTile.ConnectTo(_lastCreatedTile, randomDirection);
            _lastCreatedTile = newTile;
            if (NeedToCreateBonus())
            {
                var bonus = _bonusFactory.Create();
                newTile.SetBonus(bonus);
            }
        }
    }

    private bool NeedToCreateTile()
    {
        return _tileCreationAssistant.IsNeedToCreateNewTile(_lastCreatedTile);
    }

    private bool NeedToCreateBonus()
    {
        return _bonusCreationAssistant.IsNeedToCreateBonus();
    }
}