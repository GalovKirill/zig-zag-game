using UnityEngine;
using Zenject;

public class CubeTile : Tile, IPoolable<IMemoryPool>
{
    private IMemoryPool _pool;
    private Bonus _bonus;
    
    private const float TileWidth = 1f;

    public void OnSpawned(IMemoryPool p1)
    {
        _pool = p1;
    }

    public override void SetBonus(Bonus bonus)
    {
        bonus.SetPosition(GetBonusPosition());
        _bonus = bonus;
        bonus.transform.SetParent(transform.GetChild(0));
    }

    private Vector3 GetBonusPosition()
    {
        return Position;
    }

    protected override void Connect(Vector3 connectPoint, Direction direction)
    {
        Position = connectPoint + DirectionToOffset(direction);
    }

    protected override Vector3 GetConnectPoint(Direction direction)
    {
        var connectPoint = Position + DirectionToOffset(direction);
        return connectPoint;
    }

    /// <summary>
    /// Возвращает смещение относительно центра куба в зависимости от направления direction
    /// </summary>
    private Vector3 DirectionToOffset(Direction direction)
    {
        var offsetDirection = direction == Direction.Forward ? transform.forward : transform.right;
        return TileWidth / 2f * offsetDirection;
    }

    /// <summary>
    ///     Возвращает объект в пул. Вызывается после анимации падения.
    /// </summary>
    public void Despawn()
    {
        if(_pool != null)
           _pool.Despawn(this);
        else
            Destroy(this.gameObject);       
    }

    public void OnDespawned()
    {
        _pool = null;
        transform.GetChild(0).localPosition = -0.5f * Vector3.up;
        if(_bonus) Destroy(_bonus.gameObject);
    }

    public class Factory : PlaceholderFactory<CubeTile>
    {
    }

    public class CubeTilePool : MonoPoolableMemoryPool<IMemoryPool, CubeTile>
    {
    }
}